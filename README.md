# Projet 04 : "Tournoi d'échecs"
Mise en place d'une architecture MVC sous `Python 3.8`  
Enoncé du projet sur OpenClassRooms : [`Développez un programme logiciel en Python`](https://openclassrooms.com/fr/paths/518/projects/834/assignment)


## Démarrage rapide
```bash
git clone https://gitlab.com/HarkGit/ocr-p04.git
cd ocr-p04
python3 -m venv .venv
. .venv/bin/activate
python -m pip install -r requirements.txt
python start.py
```


## Fonctionnalités

### Edition
* Acteurs :
    - Création
    - Mise à jour du classement elo
* Joueurs d'un tournoi :
    - Création
    - Suppression
    - Mise à jour des résultats de matchs
* Tournois :
    - Création
    - Mise en place d'une liste de rondes
    - Mise à jour à partir de la ronde actuelle
* Rondes :
    - Mise en place d'une liste de parties entre joueurs
* Parties :
    - Implémentation du système Suisse pour définir les joueurs s'opposant

### Rapports
* Liste de tous les acteurs :
    - par ordre alphabétique
    - par classement
* Liste de tous les joueurs d'un tournoi :
    - par ordre alphabétique
    - par classement
* Liste de tous les tournois
* Liste des rondes d'un tournoi
* Liste de toutes les parties d'un tournoi, par ronde


## Dépendances
voir le fichier [`requirements.txt`](requirements.txt)


## Data Structure
généré avec sqlalchemy_schemadisplay
![chess_tournament.png](https://i.imgur.com/MUSlGPO.png "chess_tournament.db structure")


## Flake8 report
Le rapport d'analyse du code est disponible au format html dans le répertoire [`flake8-report`](flake8-report/index.html)
Il peut être regénéré via la commande :
```bash
flake8 .
```


## Démonstration
Le script [`populate.py`](demo/populate.py) permet de générer des données en base :
    - 4 tournois
    - 9 acteurs
    - 8 joueurs inscrits au tournoi 1
Il permet également de générer la base même si elle n'est pas encore créée
```bash
python demo/populate.py
```


## TODO
* sqlalchemy : gérer les suppressions en cascade
* sqlalchemy : créer la base à partir de différents fichiers-modèles pour simplifier l'écriture
* récursivité : résoudre les rondes de victoires/défaites sans solution (boucle infinie)
* menu : afficher le niveau des menus (ex: Rapports > Données d'un tournoi > Ronde en cours)
* tests : non implémentés dans cette version
