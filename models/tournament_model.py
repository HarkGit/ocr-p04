# tournament_model.py créé par GGT le 2022.06.05--22:24:08
"""Fonctionnalités d'accès à la table tournaments"""

from sqlalchemy import and_
from sqlalchemy.exc import NoResultFound

from models.db_create import Tournament, session
from models.library_model import read_lib, read_all_lib


def create(tournament):
    """\
    Fonction qui crée un tournoi à partir d'un objet TournamentController
    et renvoie un objet Tournament.
    """
    tournament_model = Tournament(
        name=tournament.name,
        place=tournament.place,
        date_start=tournament.date_start,
        date_end=tournament.date_end,
        mode=tournament.mode,
        description=tournament.description,
        nb_rounds=tournament.nb_rounds,
    )
    session.add(tournament_model)
    session.commit()
    return tournament_model


def read(tournament):
    """\
    Fonction qui renvoie le modèle d'un tournoi ou None
    à partir d'un objet TournamentController ou Tournament
    """
    try:
        tournament_model = (
            session.query(Tournament)
            .filter(
                and_(
                    Tournament.name == tournament.name,
                    Tournament.place == tournament.place,
                    Tournament.date_start == tournament.date_start,
                    Tournament.date_end == tournament.date_end,
                    Tournament.mode == tournament.mode,
                    Tournament.description == tournament.description,
                    Tournament.nb_rounds == tournament.nb_rounds,
                )
            )
            .one()
        )
    except (NameError, NoResultFound):
        tournament_model = None
    return tournament_model


def read_id(id_):
    """Fonction qui renvoie les données d'un tournoi à partir d'un id"""
    return read_lib(id_, Tournament)


def read_all(order=Tournament.id) -> dict:
    """Fonction qui renvoie l'ensemble des tournois connus en base"""
    return read_all_lib(Tournament).order_by(order)


def update(tournament_model, round_model):
    """Fonction de mise à jour des scores d'une partie"""
    tournament_model.rounds.append(round_model)
    session.commit()
