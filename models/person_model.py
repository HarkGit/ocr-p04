# person_model.py créé par GGT le 2022.06.17--12:40:27
"""Fonctionnalités d'accès à la table persons"""

from sqlalchemy import and_
from sqlalchemy.exc import NoResultFound

from models.db_create import Person, session
from models.library_model import read_lib, read_all_lib


def create(person):
    """\
    Fonction qui crée une personne et son classement, à partir d'un objet PersonController
    et renvoie un objet Person.
    """
    person_model = Person(
        lastname=person.lastname,
        firstname=person.firstname,
        birthday=person.birthday,
        gender=person.gender,
        elo=person.elo,
    )
    session.add(person_model)
    session.commit()
    return person_model


def read(person):
    """\
    Fonction qui vérifie l'existence d'un enregistrement en base
    de type PersonController ou Person et renvoie le modèle s'il existe ou None.
    """
    try:
        person_model = (
            session.query(Person)
            .filter(
                and_(
                    Person.lastname == person.lastname,
                    Person.firstname == person.firstname,
                    Person.birthday == person.birthday,
                )
            )
            .one()
        )
    except NoResultFound:
        person_model = None
    return person_model


def read_id(id_):
    """Fonction qui renvoie les données d'une personne à partir d'un id"""
    return read_lib(id_, Person)


def read_all(order=Person.lastname) -> dict:
    """Fonction qui renvoie l'ensemble des personnes connues en base"""
    return read_all_lib(Person).order_by(order)


def update(person, new_elo, attr=Person.elo):
    """Par défaut, mise à jour du classement elo d'une personne à partir d'un objet PersonController"""
    session.query(Person).filter(Person.id == person.id).update(
        {attr: new_elo}, synchronize_session=False
    )
    session.flush()
    session.commit()
