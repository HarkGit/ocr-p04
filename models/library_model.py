# library_model.py créé par GGT le 2022.006.11--20:35:39
"""Bibliothèque de fonctionnalités concernant l'accès aux tables"""

from models.db_create import session


def read_all_lib(table) -> dict:
    """Fonction qui renvoie l'ensemble des enregistrements d'une table"""
    return session.query(table)


def read_lib(id_, table):
    """Fonction qui renvoie l'enregistrement d'une table Table à partir d'un id"""
    return read_all_lib(table).get(id_) if id_ else None
