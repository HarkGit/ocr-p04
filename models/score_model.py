# score_model.py créé par GGT le 2022.07.10--17:14:56
"""Fonctionnalités d'accès à la table scores"""

from sqlalchemy import and_
from sqlalchemy.exc import NoResultFound

from models.db_create import Score, session


def create(score):
    """Fonction qui crée un score et renvoie un objet Score"""
    score_model = Score(
        player_tournament_id=score.player.tournament_id,
        game_id=score.game_id,
        player_person_id=score.player.person_id,
        value=score.value,
    )
    session.add(score_model)
    session.commit()
    return score_model


def read(score):
    """\
    Fonction qui vérifie l'existence d'un enregistrement de type ScoreController ou Score
    et renvoie le modèle s'il existe ou None.
    """
    try:
        score_model = (
            session.query(Score)
            .filter(
                and_(
                    Score.player_tournament_id == score.player.tournament_id,
                    Score.game_id == score.game_id,
                    Score.player_person_id == score.player.person_id,
                )
            )
            .one()
        )
    except NoResultFound:
        score_model = None
    return score_model


def read_game(id_):
    """\
    Fonction qui vérifie l'existence d'un enregistrement à partir de l'id d'une partie
    et renvoie le modèle s'il existe ou None.
    """
    try:
        scores_model = (
            session.query(Score)
            .filter(
                Score.game_id == id_,
            )
            .all()
        )
    except NoResultFound:
        scores_model = None
    return scores_model
