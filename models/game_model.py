# game_model.py créé par GGT le 2022.06.16--15:13:13
"""Fonctionnalités d'accès à la table games"""

from sqlalchemy.exc import NoResultFound

from models.db_create import Game, session


def create(game):
    """Fonction qui crée une partie et renvoie un objet Game"""
    game_model = Game(
        round_id=game.round_id,
    )
    session.add(game_model)
    session.commit()
    return game_model


def read(game):
    """\
    Fonction qui vérifie l'existence d'un enregistrement en base
    de type GameController ou Game et renvoie le modèle s'il existe ou None.
    """
    try:
        game_model = (
            session.query(Game)
            .filter(
                Game.id == game.id,
            )
            .one()
        )
    except NoResultFound:
        game_model = None
    return game_model


def read_all(round_):
    """Fonction qui renvoie sous forme de liste toutes les rondes d'un tournoi"""
    try:
        games_model = (
            session.query(Game)
            .filter(
                Game.round_id == round_.id,
            )
            .all()
        )
    except NoResultFound:
        games_model = None
    return games_model


def update(game_model, score_model):
    """Fonction de mise à jour des scores d'une partie"""
    game_model.scores.append(score_model)
    session.commit()
