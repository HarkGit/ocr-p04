# player_model.py créé par GGT le 2022.05.17--22:43:58
"""Fonctionnalités d'accès à la table players"""

from typing import List

from sqlalchemy import and_
from sqlalchemy.exc import NoResultFound

from models.db_create import Player, session


def create(player):
    """\
    Fonction qui crée un joueur de tournoi en base
    à partir d'un objet PlayerController"""
    player_model = Player(
        tournament_id=player.tournament_id,
        person_id=player.person_id,
        points=0.0,
    )
    session.add(player_model)
    session.commit()


def create_ids(tournament_id, person_id):
    """\
    Fonction qui crée un joueur de tournoi en base
    à partir de ses ids"""
    player_model = Player(
        tournament_id=tournament_id,
        person_id=person_id,
        points=0.0,
    )
    session.add(player_model)
    session.commit()


def read(player):
    """\
    Fonction qui vérifie l'existence d'un enregistrement de type Player
    et le renvoie s'il existe ou None.
    """
    try:
        player_model = (
            session.query(Player)
            .filter(
                and_(
                    Player.tournament_id == player.tournament_id,
                    Player.person_id == player.person_id,
                )
            )
            .one()
        )
    except (NameError, NoResultFound):
        player_model = None
    return player_model


def read_ids(tournament_id, person_id):
    """\
    Fonction qui vérifie l'existence d'un enregistrement à partir de ses ids
    et le renvoie s'il existe ou None.
    """
    try:
        player_model = (
            session.query(Player)
            .filter(
                and_(
                    Player.tournament_id == tournament_id,
                    Player.person_id == person_id,
                )
            )
            .one()
        )
    except (NameError, NoResultFound):
        player_model = None
    return player_model


def read_all(tournament) -> List[Player]:
    """Fonction qui renvoie sous forme de set tous les joueurs enregistrés sur un tournoi"""
    try:
        players_model = (
            session.query(Player)
            .filter(
                Player.tournament_id == tournament.id,
            )
            .all()
        )
    except (NameError, NoResultFound):
        players_model = []
    return players_model


def read_points(player):
    """Fonction qui renvoie le total de points d'un joueur pour un tournoi"""
    try:
        player_points = (
            session.query(Player.points)
            .filter(
                and_(
                    Player.tournament_id == player.tournament_id,
                    Player.person_id == player.person_id,
                )
            )
            .one()[0]
        )
    except NoResultFound:
        player_points = 0.0
    return float(player_points)


def update_points(player, points):
    """Fonction qui met à jour le total de points d'un joueur pour un tournoi"""
    points += read_points(player)
    session.query(Player).filter(
        and_(
            Player.tournament_id == player.tournament_id,
            Player.person_id == player.person_id,
        )
    ).update({Player.points: points}, synchronize_session=False)
    session.flush()
    session.commit()


def delete(player):
    """Fonction qui supprime un enregistrement joueur de la base"""
    player_model = (
        session.query(Player)
        .filter(
            Player.tournament_id == player.tournament_id,
            Player.person_id == player.person_id,
        )
        .one()
    )
    session.delete(player_model)
    session.commit()
