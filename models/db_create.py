# db_create.py créé par GGT le 2022.05.15--21:12:48
"""ORM de création de la base du projet"""

from dataclasses import dataclass
from typing import List

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    create_engine,
    Column,
    Integer,
    Float,
    String,
    ForeignKey,
    ForeignKeyConstraint,
)
from sqlalchemy.orm import Session, relationship


Base = declarative_base()
db_name = "chess_tournament"


@dataclass
class Person(Base):
    """Définition de la table persons"""

    __tablename__ = "persons"
    id: int = Column(Integer, primary_key=True, autoincrement=True)

    # Person-Player: one
    players = relationship("Player", back_populates="person")

    lastname: str = Column(String(25), nullable=False)
    firstname: str = Column(String(25))
    birthday: str = Column(String)
    gender: str = Column(String(1))
    elo: int = Column(Integer, nullable=False)

    def __str__(self) -> str:
        player_suffix = "r" if self.gender.lower() == "m" else "se"
        return f"Joueu{player_suffix:2} {self.firstname.capitalize()} {self.lastname.upper()} ({self.elo} elo)"


@dataclass
class Player(Base):
    """Définition de la table players"""

    __tablename__ = "players"
    tournament_id: int = Column(Integer, ForeignKey("tournaments.id"), primary_key=True)
    person_id: int = Column(Integer, ForeignKey("persons.id"), primary_key=True)

    # Person-Player: to many
    person = relationship("Person", back_populates="players")
    # Tournament-Player: to many
    tournament = relationship("Tournament", back_populates="players")
    # Player-Score: one
    scores = relationship("Score", back_populates="player")

    points: float = Column(Float)


@dataclass
class Score(Base):
    """Définition de la table scores"""

    __tablename__ = "scores"
    player_tournament_id = Column(Integer, primary_key=True)
    game_id: int = Column(Integer, ForeignKey("games.id"), primary_key=True)
    player_person_id = Column(Integer, primary_key=True)

    __table_args__ = (
        ForeignKeyConstraint(
            ["player_tournament_id", "player_person_id"],
            ["players.tournament_id", "players.person_id"],
        ),
    )

    # Game-Score: to many
    game = relationship("Game", back_populates="scores")
    # Player-Score: to many
    player: Player = relationship("Player", back_populates="scores")

    value: float = Column(Float, nullable=False)


@dataclass
class Game(Base):
    """Définition de la table games"""

    __tablename__ = "games"
    id: int = Column(Integer, primary_key=True, autoincrement=True)

    # Round-Game: to many
    round_ = relationship("Round", back_populates="games")
    # Game_score : one
    scores: List[Score] = relationship("Score", back_populates="game")

    round_id: int = Column(Integer, ForeignKey("rounds.id"))


@dataclass
class Round(Base):
    """Définition de la table rounds"""

    __tablename__ = "rounds"
    id: int = Column(Integer, primary_key=True, autoincrement=True)

    # Tournament-Round: to many
    tournament = relationship("Tournament", back_populates="rounds")
    # Round-Game: one
    games: List[Game] = relationship("Game", back_populates="round_")

    tournament_id: int = Column(Integer, ForeignKey("tournaments.id"))
    name: str = Column(String(14))
    date_start: str = Column(String)
    date_end: str = Column(String)

    def __str__(self) -> str:
        return self.name


@dataclass
class Tournament(Base):
    """Définition de la table tournaments"""

    __tablename__ = "tournaments"
    id: int = Column(Integer, primary_key=True, autoincrement=True)

    # Tournament-Player: one
    players: List[Player] = relationship("Player", back_populates="tournament")
    # Tournament-Round: one
    rounds: List[Round] = relationship("Round", back_populates="tournament")

    name: str = Column(String(25))
    place: str = Column(String(25))
    date_start: str = Column(String)
    date_end: str = Column(String)
    mode: str = Column(String(10), nullable=False)
    description: str = Column(String(98))
    nb_rounds: int = Column(Integer)

    def __str__(self) -> str:
        return f"{self.name.title()} ({self.place.capitalize()}, {self.date_start}): {self.mode}"


engine = create_engine(f"sqlite:///{db_name}.db")
Base.metadata.create_all(engine)

session = Session(engine)

# $ sqlite3 chess_tournament.db
# sqlite> .tables           => games        players      scores
#                              persons      rounds       tournaments
# sqlite> .schema players
# sqlite> .headers on
# sqlite> .mode columns
# sqlite> select * from players;
# sqlite> select * from games;
