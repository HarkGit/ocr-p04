# round_model.py créé par GGT le 2022.06.17--11:46:54
"""Fonctionnalités d'accès à la table rounds"""

from typing import List

from sqlalchemy import and_
from sqlalchemy.exc import NoResultFound

from models.db_create import Round, session
from models.library_model import read_lib


def create(round_):
    """Fonction qui crée une ronde de tournoi et renvoie un objet Round"""
    round_model = Round(
        name=round_.name,
        date_start=round_.date_start,
        date_end=round_.date_end,
        tournament_id=round_.tournament_id,
    )
    session.add(round_model)
    session.commit()
    return round_model


def read(round_):
    """\
    Fonction qui vérifie l'existence d'un enregistrement en base
    de type RoundController ou Round et renvoie le modèle s'il existe ou None.
    """
    try:
        round_model = (
            session.query(Round)
            .filter(
                and_(
                    Round.name == round_.name,
                    Round.tournament_id == round_.tournament_id,
                )
            )
            .one()
        )
    except NoResultFound:
        round_model = None
    return round_model


def read_id(id_):
    """Fonction qui renvoie les données d'une ronde à partir d'un id"""
    return read_lib(id_, Round)


def read_all(tournament) -> List[Round]:
    """Fonction qui renvoie sous forme de liste toutes les rondes d'un tournoi"""
    try:
        rounds_model = (
            session.query(Round)
            .filter(
                Round.tournament_id == tournament.id,
            )
            .all()
        )
    except (NameError, NoResultFound):
        rounds_model = []
    return rounds_model


def update(id_, new_date, attr=Round.date_end):
    """\
    Fonction de mise à jour de la date initiale ou finale d'une ronde.
    La date finale est prise en charge par défaut
    """
    session.query(Round).filter(Round.id == id_).update(
        {attr: new_date}, synchronize_session=False
    )
    session.flush()
    session.commit()


def delete(round_model):
    """Fonction de suppression de ronde"""
    session.query(Round).filter(Round.id == round_model.id).delete(
        synchronize_session=False
    )
    session.commit()
