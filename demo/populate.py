# populate.py créé par GGT le 2022.07.11--14:06:39

from models import tournament_model, person_model
from models.db_create import session
from controllers import tournament_controller, person_controller, player_controller


with session:
    tournament1_model = tournament_model.Tournament(
        name="panam chess 2022",
        place="paris",
        date_start="2022-07-14",
        date_end="2022-07-14",
        mode="blitz",
        description="pas grand chose",
        nb_rounds=4,
    )
    tournament2 = tournament_controller.TournamentController(
        name="Chess 2022",
        place="Rosny",
        date_start="2022-05-19",
        date_end="2022-05-19",
        mode="Blitz",
        description="Rien de special",
        rounds=[],
        players=[]
    )
    tournament3 = tournament_controller.TournamentController(
        "Chess Tournament 2023",
        "Mexico",
        "2023-07-12",
        "2023-07-13",
        "bullet",
        "Nada",
        rounds=[],
        players=[]
    )
    tournament4 = tournament_controller.TournamentController(
        "Chess in Japan",
        "Tokyo",
        "2012-07-02",
        "2012-07-03",
        "Blitz",
        "Nashi",
        rounds=[],
        players=[]
    )

    tournament1_model = (
        tournament_model.create(tournament1_model)
        if not (t1 := tournament_model.read(tournament1_model))
        else t1
    )
    tournament2_model = (
        tournament_model.create(tournament2)
        if not (t2 := tournament_model.read(tournament2))
        else tournament_controller.TournamentController.get(t2.id)
    )
    tournament3_model = (
        tournament_model.create(tournament3)
        if not (t3 := tournament_model.read(tournament3))
        else tournament_controller.TournamentController.get(t3.id)
    )
    tournament4_model = (
        tournament_model.create(tournament4)
        if not (t4 := tournament_model.read(tournament4))
        else tournament_controller.TournamentController.get(t4.id)
    )

    print([str(tournament) for tournament in tournament_model.read_all()])

    albert_model = person_model.Person(
        lastname="ein",
        firstname="albert",
        birthday="1879-03-14",
        gender="m",
        elo=1500,
    )
    clem = person_controller.PersonController("ggt", "clem", "1972-09-03", "m", 1832)
    fred = person_controller.PersonController("MAS", "Fred", "1973-06-09", "M", 1654)

    albert_model = (
        person_model.create(albert_model)
        if (p1 := person_model.read(albert_model)) is None
        else p1
    )
    clem_model = (
        person_model.create(clem)
        if (p2 := person_model.read(clem)) is None
        else person_controller.PersonController.get(p2.id)
    )
    fred_model = (
        person_model.create(fred)
        if (p3 := person_model.read(fred)) is None
        else person_controller.PersonController.get(p3.id)
    )

    # albe = person_controller.PersonController("ein", "albert", "1879-03-14", "m", 1500)
    tony = person_controller.PersonController("BPI", "Tony", "1997-12-07", "F", 1792)
    beth = person_controller.PersonController("harmon", "beth", "1948-11-02", "f", 2791)
    gary = person_controller.PersonController("kasparov", "garry", "1963-04-13", "m", 2851)
    jimy = person_controller.PersonController("bta", "jimmy", "1992-07-04", "m", 1535)
    jeje = person_controller.PersonController("blr", "jerome", "1973-10-22", "m", 1492)
    pipa = person_controller.PersonController("Hiha", "pipa", "1971-01-01", "f")

    # albe_model = (
    #   person_model.create(albe)
    #   if (p1 := person_model.read(albe)) is None
    #   else person_controller.PersonController.get(p1.id)
    # )
    tony_model = (
        person_model.create(tony)
        if (p4 := person_model.read(tony)) is None
        else person_controller.PersonController.get(p4.id)
    )
    beth_model = (
        person_model.create(beth)
        if (p5 := person_model.read(beth)) is None
        else person_controller.PersonController.get(p5.id)
    )
    gary_model = (
        person_model.create(gary)
        if (p6 := person_model.read(gary)) is None
        else person_controller.PersonController.get(p6.id)
    )
    jimy_model = (
        person_model.create(jimy)
        if (p7 := person_model.read(jimy)) is None
        else person_controller.PersonController.get(p7.id)
    )
    jeje_model = (
        person_model.create(jeje)
        if (p8 := person_model.read(jeje)) is None
        else person_controller.PersonController.get(p8.id)
    )
    pipa_model = (
        person_model.create(pipa)
        if (p9 := person_model.read(pipa)) is None
        else person_controller.PersonController.get(p9.id)
    )

    players_id = player_controller.PlayerController.new_ids("1 2 5 47 3 fg   '' -5 5 3 4 6 7 8")
    tournament_controller.TournamentController.get(1).addplayers(players_id)

    print(tournament_controller.TournamentController.get(1).players)


# Exemple
# sequence complète de tournoi : tournoi 1
# round 1
#   0  1  1  .5
# round 2
#   1  1  1  0
# round 3
#   1  0  1  .5
# round 4
#   0  0 .5  1
# => classement :  6 5 2 1 4 7 8 3
