# person_controller.py créé par GGT le 2022.05.16--16:37:22
"""Gestion du pool de participants"""

from dataclasses import dataclass, asdict
from typing import ClassVar

from sqlalchemy.exc import NoResultFound

from models.db_create import Person
from models.person_model import create, read, read_id, read_all, update
from views.library_view import display, print_dict
from controllers.library_controller import (
    validate,
    check_name,
    check_birthday,
    check_gender,
    check_elo,
)


@dataclass
class PersonController:
    """classe de gestion de personne"""

    lastname: str
    firstname: str
    birthday: str
    gender: str = "m"
    elo: int = 1000
    id: int = None
    ELO_MIN: ClassVar = 1000
    ELO_MAX: ClassVar = 3000

    def __post_init__(self):
        self.lastname: str = self.lastname.lower()
        self.firstname: str = self.firstname.lower()
        self.gender: str = self.gender.lower()
        try:
            self.id = read(self).id
        except (NameError, AttributeError, NoResultFound):
            self.id = None

    def __str__(self) -> str:
        person_suffix = "r" if self.gender != "f" else "se"
        return f"Joueu{person_suffix:2} {self.firstname.capitalize()} {self.lastname.upper()} ({self.elo} elo)"

    def serialize(self):
        """Méthode qui transforme un objet PersonController en objet Person"""
        if self.id is None:
            person_model = create(self)
            self.id = person_model.id
        else:
            person_model = read(self)
        return person_model

    @classmethod
    def deserialize(cls, person_model):
        """Méthode qui transforme un objet Person en objet PersonController"""
        person_data = asdict(person_model)
        return cls(**person_data)

    @classmethod
    def new(cls):
        """\
        Méthode de création alternative d'un joueur.
        Les données sont vérifiées avant de renvoyer une instance
        """
        lastname = check_name(display("Nom"))
        firstname = check_name(display("Prénom"))
        birthday = check_birthday(display("Date de naissance (YYYY-MM-DD)"))
        gender = check_gender(display("Sexe [M/F]"))
        elo = check_elo(
            display("Classement"), PersonController.ELO_MIN, PersonController.ELO_MAX
        )

        return cls(lastname, firstname, birthday, gender, elo)

    def edit_elo(self, new_elo: int):
        """Méthode de mise à jour du classement d'un joueur"""
        self.elo = new_elo
        if self.id:
            update(self, new_elo)
        return self

    @classmethod
    def get(cls, id_):
        """Méthode qui retourne un objet PersonController à partir d'un id"""
        try:
            person_data = asdict(read_id(id_))
        except TypeError:
            return None
        else:
            return cls(**person_data)

    @classmethod
    def get_all(cls, order=None):
        """Méthode qui renvoie tous les joueurs connus en base"""
        for person in read_all(order):
            print_dict(person.id, person)


def getall_orderby_name():
    """\
    Méthode qui affiche tous les joueurs connus en base
    triés par leur nom
    """
    PersonController.get_all(Person.lastname)


def getall_orderby_elo():
    """\
    Méthode qui affiche tous les joueurs connus en base
    triés par leur classement
    """
    PersonController.get_all(Person.elo.desc())


def add():
    """programme principal : action "Nouveau Joueur" du menu principal de start"""
    person = PersonController.new()
    if validate(person):
        if read(person) is None:
            create(person)
            print("  enregistrement effectué.")
        else:
            print(r"  /!\ Enregistrement déjà présent /!\ ")
    else:
        print("  Annulation")


def edit(person):
    """Mises à jour d'une personne : action "Joueur" du sous-menu d'édition"""
    if person is not None:
        new_elo = check_elo(
            display("\n  Nouveau classement elo"),
            PersonController.ELO_MIN,
            PersonController.ELO_MAX,
        )
        try:
            print(f"  {person.edit_elo(new_elo)}")
        except AttributeError:
            return
