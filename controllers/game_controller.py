# game_controller.py créé par GGT le 2022.05.16--16:55:42
"""Gestion des parties d'un tournoi"""

from dataclasses import dataclass, asdict
from typing import Tuple

from models.db_create import Game
from models.player_model import update_points
from models.score_model import create as create_score
from models.game_model import create, read
from controllers.score_controller import ScoreController


@dataclass
class GameController:
    """classe de gestion des parties d'un tournoi"""

    scores: Tuple[ScoreController]
    round_id: int = None
    id: int = None

    def __str__(self):
        results = [f"{score}" for score in self.scores]
        return "\n".join(results)

    def serialize(self):
        """Méthode qui transforme un objet GameController en objet Game"""
        if self.id is None:
            for score in self.scores:
                score.game_id = self.id
            self.scores = tuple(score.serialize() for score in self.scores)
            game_model = create(self)
        else:
            game_model = read(self)
        return game_model

    @classmethod
    def deserialize(cls, game_model):
        """Méthode qui transforme un objet Game ou dictionnaire en objet GameController"""
        game_data = asdict(game_model) if isinstance(game_model, Game) else game_model
        game_data["scores"] = [
            ScoreController.deserialize(score)
            for score in game_data["scores"]
            if isinstance(score, dict)
        ]
        return cls(**game_data)

    @classmethod
    def getscores(cls, pair):
        """Méthode d'attribution de points aux joueurs d'une partie"""
        score0 = ScoreController.play(pair[0], None)
        score1 = ScoreController(pair[1], None, 1 - score0.value)
        return (score0, score1)

    @classmethod
    def play(cls, scores, round_):
        """Création d'une partie. Une instance de partie est renvoyée"""
        game = cls(scores=None, round_id=round_.id, id=None)
        game_model = create(game)
        game.id = game_model.id

        for score in scores:
            score.game_id = game.id
            create_score(score)
            score.player.points += score.value
            update_points(score.player, score.value)

        game.scores = scores
        return game
