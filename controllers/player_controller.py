# player_controller.py créé par GGT le 2022.06.20--00:38:25
"""Gestion des participants d'un tournoi"""

from dataclasses import dataclass, field, asdict

from models.player_model import create, read, read_all, read_points, update_points
from controllers.person_controller import PersonController


@dataclass(order=True)
class PlayerController:
    """classe de gestion des participants à un tournoi"""

    value: float = field(init=False, repr=False)
    elo: int = field(init=False, repr=False)
    points: float
    tournament_id: int
    person_id: int

    def __post_init__(self):
        self.value = self.points
        self.elo = PersonController.get(self.person_id).elo
        self.points = self.__points

    def __str__(self) -> str:
        person = PersonController.get(self.person_id)
        person_suffix = "r" if person.gender != "f" else "se"
        return f"Joueu{person_suffix:2} {person.firstname.capitalize()} {person.lastname.upper()} ({person.elo} elo)"

    @property
    def __points(self):
        """Méthode qui récupère les points d'un joueur"""
        return read_points(self)

    @__points.setter
    def __points(self, new_points):
        """Méthode de mise à jour des points d'un joueur"""
        update_points(self, new_points)

    def serialize(self):
        """Méthode qui transforme un objet PlayerController en objet Player"""
        return create(self) if (player_model := read(self)) is None else player_model

    @classmethod
    def deserialize(cls, player_model):
        """Méthode qui transforme un objet Player en objet PlayerController"""
        player_data = asdict(player_model)
        return cls(**player_data)

    @classmethod
    def get_all(cls, tournament):
        """Méthode qui renvoie la liste des joueurs d'un tournoi"""
        return [
            PlayerController.deserialize(player_model)
            for player_model in read_all(tournament)
        ]

    @staticmethod
    def new_ids(players_str: str) -> set:
        """Fonction de tri des identifiants de joueurs potentiels sous forme de set"""
        return {
            int(player_id) for player_id in players_str.split() if player_id.isdigit()
        }
