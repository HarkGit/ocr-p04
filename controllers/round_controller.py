# round_controller.py créé par GGT le 2022.06.20--15:40:48
"""Gestion des rondes d'un tournoi"""

from dataclasses import dataclass, asdict
from typing import List, ClassVar
from datetime import datetime

from models.db_create import Round
from models.round_model import create, update, read, delete
from views.library_view import print_vs
from controllers.library_controller import validate
from controllers.person_controller import PersonController
from controllers.game_controller import GameController


@dataclass
class RoundController:
    """classe de gestion de rondes"""

    name: str
    date_start: str
    date_end: str
    games: List[GameController]
    tournament_id: int
    id: int = None
    ROUND_NAME: ClassVar = "Round"

    def __str__(self):
        return self.name

    def serialize(self):
        """Méthode qui transforme un objet RoundController en objet Round"""
        if self.id is None:
            if self.games:
                for game in self.games:
                    game.round_id = self.id
                self.games = [game.serialize() for game in self.games]
            round_model = create(self)
        else:
            round_model = read(self)
        return round_model

    @classmethod
    def deserialize(cls, round_model):
        """Méthode qui transforme un objet Round ou dictionnaire en objet RoundController"""
        round_data = (
            asdict(round_model) if isinstance(round_model, Round) else round_model
        )
        round_data["games"] = [
            GameController.deserialize(game)
            for game in round_data["games"]
            if isinstance(game, dict)
        ]
        return cls(**round_data)

    @classmethod
    def play(cls, tournament):
        """Création d'un round. Une instance de round est renvoyée"""
        name = f"{cls.ROUND_NAME} {len(tournament.rounds) + 1}"
        pairs = cls.set_swiss_pairs(tournament)

        print_vs(pairs)

        round_ = cls(
            name=name,
            tournament_id=tournament.id,
            games=None,
            date_start=None,
            date_end=None,
        )
        if validate(round_):
            round_.date_start = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            round_model = round_.serialize()
            round_.id = round_model.id

            list_scores = [GameController.getscores(pair) for pair in pairs]
            # print_results(list_scores)

            if validate(f"Enregistrement de {round_}"):
                round_.games = [
                    GameController.play(scores, round_) for scores in list_scores
                ]
                return round_
            else:
                delete(round_model)

    @classmethod
    def set_swiss_pairs(cls, tournament):
        """Méthode qui associe des pairs de joueurs en suivant le système suisse"""
        pairs_available = tournament.get_pairs_available()
        ranking = RoundController.sort_players(tournament)
        # print([player.person_id for player in ranking])
        nb_players = len(tournament.players)
        nb_half_players = nb_players // 2

        pairs = []
        for i in range(nb_half_players):
            j = nb_half_players
            while True:
                pair = (
                    (ranking[i], ranking[j])
                    if ranking[i].person_id < ranking[j].person_id
                    else (ranking[j], ranking[i])
                )
                # print((pair[0].person_id, pair[1].person_id))
                players_picked = [player for pair in pairs for player in pair]
                if (
                    pair in pairs_available
                    and pair[0] not in players_picked
                    and pair[1] not in players_picked
                ):
                    pairs.append(pair)
                    pairs_available.remove(pair)
                    j = nb_half_players
                    break
                j += 1
                j %= nb_players
                # print([player.person_id for pair in pairs for player in pair])
        return pairs

    @staticmethod
    def sort_players(tournament) -> list:
        """Méthode de tri des joueurs en fonction de leurs points et de leur classement"""
        return sorted(
            tournament.players,
            key=lambda player: [
                player.points,
                PersonController.get(player.person_id).elo,
            ],
            reverse=True,
        )

    def get_pairs(self):
        """Méthode qui renvoie tous les joueurs d'une ronde, par partie"""
        return [
            ((game.scores[0]).player, (game.scores[1]).player) for game in self.games
        ]

    def finalize(self):
        """Méthode qui met à jour la date de fin d'une ronde et renvoie l'objet ronde"""
        self.date_end = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        update(self.id, self.date_end)
        return self
