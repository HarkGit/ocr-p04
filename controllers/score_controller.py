# score_controller.py créé par GGT le 2022.07.10--17:08:04
"""Gestion des scores d'un tournoi"""

from dataclasses import dataclass, asdict
from typing import ClassVar

from models.db_create import Score
from models.score_model import create, read
from views.library_view import display
from controllers.player_controller import PlayerController


@dataclass
class ScoreController:
    """classe de gestion des scores d'un tournoi"""

    player: PlayerController
    game_id: int = None
    value: float = 0.0
    POINTS: ClassVar = (0.0, 0.5, 1.0)

    def __str__(self):
        return f"{str(self.player):>40} : {self.value} (cumul {self.player.points})"

    def serialize(self):
        """Méthode qui transforme un objet ScoreController en objet Score"""
        score = self
        score.player = self.player.serialize()
        return create(score) if (score_model := read(score)) is None else score_model

    @classmethod
    def deserialize(cls, score_model):
        """Méthode qui transforme un objet Score ou dictionnaire en objet ScoreController"""
        score_data = (
            asdict(score_model) if isinstance(score_model, Score) else score_model
        )
        if isinstance(score_data["player"], dict):
            score_data["player"] = PlayerController(**score_data["player"])
        return cls(**score_data)

    @classmethod
    def play(cls, player, game_id):
        """Création d'un score. Une instance de score est renvoyée"""
        while True:
            value = display(f"{player} : points [0, 0.5, 1]", "?")
            try:
                value = float(value)
            except ValueError:
                continue
            if value in cls.POINTS:
                score = cls(player, game_id, value)
                return score
