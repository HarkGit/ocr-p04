# library_controller.py créé par GGT le 2022.06.07--10:44:17
"""Bibliothèque de fonctionnalités concernant la gestion d'un tournoi"""

import re
from datetime import date

from views.library_view import display, print_dict


def validate(object_controller: object) -> bool:
    """\
    Fonction qui donne le choix de pérenniser en base une instance (joueur, tournoi)
    plutôt que de créer automatiquement une instance dès l'init.
    """
    yesno = display(f"\n{object_controller}\nValider", "[O/n] ?")
    return True if yesno.lower() in ["", "o"] else False


def get(st: str, class_: object) -> int:
    """Fonction qui teste et renvoie une instance valide de la classe class_"""
    while True:
        id_ = display(st, "?")
        try:
            id_ = int(id_)
            object_ = class_.get(id_)
            print(f"\n  {object_}", end="")
            break
        except (TypeError, ValueError):
            return None
    return object_


def get_all(class_: object, order=None):
    """Méthode qui affiche tous les objets de la classe class_ connus en base"""
    for id_, object_ in class_.get_all(order).items():
        print_dict(id_, object_)


def check_name(st: str, max_: int = 25) -> str:
    """Vérification des données : générique"""
    model = r"[0-9A-Za-z' .,-]{2," f"{max_}" r"}"
    while True:
        if re.fullmatch(model, st):
            break
        st = display(f"  entre 2 et {max_} lettres")
    return st.lower()


def check_birthday(st: str) -> str:
    """Vérification des données : date de naissance"""
    while True:
        try:
            date_ = date.fromisoformat(st)
        except ValueError:
            st = display("  format date invalide : YYYY-MM-DD")
            continue

        if isinstance(date_, date) and date_ < date.today():
            break
        st = display("  trop jeune")
    return st


def check_gender(st: str) -> str:
    """Vérification des données : sexe"""
    while True:
        if st.lower() in ["m", "f"]:
            break
        st = display("  M ou F")
    return st.lower()


def check_elo(nb: str, min_: int, max_: int) -> int:
    """Vérification des données : classement elo"""
    while True:
        if nb.isdigit() and min_ <= int(nb) <= max_:
            nb = int(nb)
            break
        nb = display(f"  entre {min_} et {max_}")
    return nb


def check_date(st: str, date_start: str = None) -> str:
    """Vérification des données : dates de tournoi"""
    while True:
        try:
            date_ = date.fromisoformat(st)
        except ValueError:
            if st or date_start is None:
                st = display("  format date invalide : YYYY-MM-DD")
                continue
            st = date_start
        else:
            if date_start is None:
                break
            if date_start and date_ >= date.fromisoformat(date_start):
                break
            st = display("  date invalide")
    return st


def check_mode(st: str, modes: tuple) -> str:
    """Vérification des données : mode"""
    while True:
        if st.lower() in modes:
            break
        st = display(f"  {', '.join(modes)}")
    return st.lower()


def check_number(nb: str, default: int = None) -> int:
    """Vérification des données : nombre de joueurs ou de rounds"""
    # TODO : à modifier suivant le nb de joueurs
    while True:
        if not nb and (default is not None):
            nb = default
            break
        if nb.isdigit():
            nb = int(nb)
            break
        nb = display("  un nombre")
    return nb
