# tournament_controller.py créé par GGT le 2022.05.17--10:42:27
"""Gestion des tournois"""

from dataclasses import dataclass, asdict
from typing import ClassVar, List
from itertools import combinations

from sqlalchemy.exc import IntegrityError

from models.db_create import Tournament
from models.player_model import read_ids as read_player, delete
from models.tournament_model import create, read, read_id, read_all
from views.library_view import display, print_dict, print_vs
from controllers.library_controller import validate, check_name, check_date, check_mode
from controllers.person_controller import PersonController
from controllers.player_controller import PlayerController
from controllers.round_controller import RoundController


@dataclass
class TournamentController:
    """classe de gestion de tournoi"""

    name: str
    place: str
    date_start: str
    date_end: str
    mode: str
    description: str
    rounds: List[RoundController]
    players: List[PlayerController]
    id: int = None
    nb_rounds: int = 4
    modes: ClassVar = ("bullet", "blitz", "quick")

    def __post_init__(self):
        self.name = self.name.lower()
        self.place = self.place.lower()
        self.mode = self.mode.lower()
        self.description = self.description.lower()
        self.players = PlayerController.get_all(self)

    def __str__(self) -> str:
        st = f"{self.name.title()} ({self.place.capitalize()}, {self.date_start}): {self.mode}"
        if self.players:
            st += f"\n    {len(self.players)} joueurs, {self.nb_rounds} rounds."
        return st

    def serialize(self):
        """Méthode qui transforme un objet TournamentController en objet Tournament"""
        if self.id is None:
            tournament_model = create(self)
        else:
            tournament = self
            tournament.players = [player.serialize() for player in self.players]
            for round_ in self.rounds:
                round_.tournament_id = self.id
            tournament.rounds = [round_.serialize() for round_ in self.rounds]
            tournament_model = read(tournament)
        return tournament_model

    @classmethod
    def deserialize(cls, tournament_model):
        """Méthode qui transforme un objet Tournament ou dictionnaire en objet TournamentController"""
        tournament_data = (
            asdict(tournament_model)
            if isinstance(tournament_model, Tournament)
            else tournament_model
        )
        tournament_data["players"] = [
            PlayerController(**player_data)
            for player_data in tournament_data["players"]
            if isinstance(player_data, dict)
        ]
        tournament_data["rounds"] = [
            RoundController.deserialize(round_)
            for round_ in tournament_data["rounds"]
            if isinstance(round_, dict)
        ]
        return cls(**tournament_data)

    @classmethod
    def new(cls):
        """Création d'un tournoi. Une instance de tournoi est renvoyée"""
        name = check_name(display("Nom du tournoi"))
        place = check_name(display("Lieu"))
        date_start = check_date(display("Début du tournoi (YYYY-MM-DD)"))
        date_end = check_date(display(f"Fin du tournoi ({date_start})"), date_start)
        mode = check_mode(
            display(f"mode ({', '.join(TournamentController.modes)})"),
            TournamentController.modes,
        )
        description = check_name(display("Description"), 98)
        rounds = []
        players = []
        return cls(
            name, place, date_start, date_end, mode, description, rounds, players
        )

    def play(self):
        """Méthode de gestion de l'avancée du tournoi"""
        if len(self.players) < 8:
            print("  Pas assez de joueurs inscrits\n  Valider 2 pour inscrire des joueurs")
            return
        if not self.rounds or (
            self.rounds
            and (round_ := self.rounds[-1]).date_end
            and (len(self.rounds) < self.nb_rounds)
        ):
            new_round = RoundController.play(self)
            if new_round:
                self.rounds.append(new_round)
        else:
            if not round_.date_end:
                new_round = round_
            else:
                new_round = None
                players = [str(player) for player in RoundController.sort_players(self)]
                print("Tournoi terminé !")
                print(f"Classement : {players}")
        if new_round and new_round.date_start:
            self.rounds[-1] = new_round.finalize()

    def addplayers(self, persons_id):
        """Fonction d'ajouts de joueurs pour un tournoi donné"""
        for person_id in persons_id:
            try:
                if PersonController.get(person_id):
                    player = PlayerController(0.0, self.id, person_id)
                    if player not in self.players:
                        player.serialize()
                        self.players.append(player)
                        print_dict(person_id, f"{player} : OK !")
                    else:
                        print_dict(person_id, f"Identifiant déjà enregistré ({player})")
                else:
                    print_dict(person_id, "Identifiant inconnu")
            except AttributeError:
                print_dict(person_id, "Identifiant inconnuuu")
            except IntegrityError:
                print_dict(person_id, f"Identifiant déjà enregistré ({player})")

    def removeplayer(self):
        """Fonction de suppression de l'identifiant d'un joueur pour un tournoi donné"""
        player_id = display("Identifiant du joueur à retirer du tournoi")
        person_id = int(player_id) if player_id.isdigit() else 0
        try:
            player = PlayerController.deserialize(read_player(self.id, person_id))
            if player in self.players:
                delete(player)
                self.players.remove(player)
                print_dict(person_id, f"{player} : supprimé !")
            else:
                print_dict(
                    player.person_id, f"Joueur non inscrit au tournoi ({player})"
                )
        except TypeError:
            print_dict(player_id, "Identifiant inconnu")

    @classmethod
    def get(cls, id_):
        """Méthode qui retourne un objet TournamentController à partir d'un id"""
        tournament_model = read_id(id_)
        return cls.deserialize(tournament_model)

    @classmethod
    def get_all(cls, order=None):
        """Méthode qui renvoie tous les tournois connus en base"""
        for tournament in read_all(order):
            print_dict(tournament.id, tournament)

    def get_all_orderby(self, ksort=lambda person: person.lastname, reverse=False):
        """Fonction qui renvoie une liste ordonnée des joueurs d'un tournoi"""
        persons = [PersonController.get(player.person_id) for player in self.players]
        persons.sort(key=ksort, reverse=reverse)
        for person in persons:
            print_dict(person.id, person)

    def get_pairs_available(self) -> List[tuple]:
        """Méthode qui renvoie les possibilités de rencontres encore disponibles entre joueurs"""
        pairs_available = list(combinations(self.players, 2))
        if self.rounds:
            pairs_picked = []
            for round_ in self.rounds:
                pairs = round_.get_pairs()
                pairs_picked.extend(pairs)
            pairs_available = [
                pair for pair in pairs_available if pair not in pairs_picked
            ]
            # print([(pair[0].person_id, pair[1].person_id) for pair in pairs_available])
        return pairs_available

    def getall_rounds(self, with_encounters=False):
        """Méthode qui renvoie les rondes et les parties d'un tournoi"""
        for round_ in self.rounds:
            print(round_)
            if with_encounters:
                players = [
                    (game.scores[0].player, game.scores[1].player)
                    for game in round_.games
                ]
                print_vs(players)


def add():
    """programme principal : action "Nouveau Tournoi" du menu principal de start"""
    tournament = TournamentController.new()
    if validate(tournament):
        if read(tournament) is None:
            create(tournament)
            print("  enregistrement effectué.")
        else:
            print(r"  /!\ Enregistrement déjà présent /!\ ")
    else:
        print("  Annulation")


def addplayers(tournament):
    """Fonction d'interaction qui permet l'ajout de joueurs pour un tournoi donné"""
    players_str = display("Identifiants des joueurs du tournoi")
    persons_id = PlayerController.new_ids(players_str)
    return tournament.addplayers(persons_id)


def getall_players_orderby_name(tournament):
    """Fonction qui renvoie une liste ordonnée alphabétiquement des joueurs d'un tournoi"""
    return tournament.get_all_orderby()


def getall_players_orderby_elo(tournament):
    """Fonction qui renvoie une liste classée par elo des joueurs d'un tournoi"""
    return tournament.get_all_orderby(ksort=lambda person: person.elo, reverse=True)


def getall_matchs(tournament):
    """Fonction qui renvoie les rondes et les parties d'un tournoi"""
    return tournament.getall_rounds(with_encounters=True)
