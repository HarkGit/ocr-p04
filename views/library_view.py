# library_view.py créé par GGT le 2022.06.07--10:53:05
"""Bibliothèque de fonctionnalités concernant l'affichage du projet"""

from typing import List


def display(st: str, punct: str = ":") -> str:
    """Récupération des données du tournoi"""
    return input(f"{st} {punct} ")


def print_dict(id_: int, object_: str) -> None:
    """Affichage des informations essentielles d'un objet (tournoi, joueur)"""
    print(f" {id_:>3}> {object_}")


def print_vs(pairs: List[tuple]) -> None:
    """Affichage des rencontres entre joueurs"""
    for pair in pairs:
        print(f"  {str(pair[0]):35} vs   {pair[1]}")


def print_results(list_scores: List[tuple]) -> None:
    """Affichage des résultats de parties entre joueurs"""
    results = [f"{score}" for scores in list_scores for score in scores]
    print("\n".join(results))
