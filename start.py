# main.py créé par GGT le 2022.05.16--17:01:36
"""Programme principal du jeu : sélection de l'action à entreprendre"""

import sys
from typing import Any

from models.db_create import session
from controllers import tournament_controller, person_controller, library_controller


# breadcrumbs = []

PRINCIPAL = ("Menu Tournoi", "Menu Joueur", "Quitter")
PERSON = (
    "Liste alphabétique des joueurs",
    "Classement des joueurs",
    "Ajouter un joueur",
    "Joueur : mise à jour du classement",
    "Retour",
    "Quitter",
)
TOURNAMENT = (
    "Liste des tournois",
    "Ajouter un tournoi",
    "Données d'un tournoi",
    "Retour",
    "Quitter",
)
TOURNAMENT_MENU = (
    "Poursuite du tournoi",
    "Inscrire des joueurs",
    "Supprimer un joueur",
    "Liste alphabétique des joueurs",
    "Classement des joueurs",
    "Rondes",
    "matchs",
    "Retour",
    "Quitter",
)

# def get_breadcrumbs(choices: tuple, actions: tuple, rep: int) -> str:
#     """"""
#     if actions[rep-1] == "":
#         breadcrumbs.pop()
#     elif actions[rep-1] == "Quitter":
#         breadcrumbs.clear()
#     else:
#         breadcrumbs.append(choices[rep-1])
#     return " > ".join(breadcrumbs)


def listing(choices: tuple) -> str:
    """Fonction qui définit les choix d'un menu à partir d'un itérable"""
    lines = ""
    for i, line in enumerate(choices):
        lines += f"\n{i+1:>7}. {line}"
    return lines


def menu(choices: tuple, actions: tuple, object_: Any = None) -> None:
    """Fonction de redirection suite à un choix"""
    select = listing(choices)
    while True:
        print(select)
        rep = input(f"Choix [1..{len(choices)}] : ")

        if not rep.isdigit() or int(rep) not in range(1, len(choices) + 1):
            print(r"/!\ entre 1 et " f"{len(choices)}" r"/!\ ")
        else:
            rep = int(rep)
            # print(f"\n{get_breadcrumbs(choices, actions, rep)}")
            try:
                if object_ is None:
                    actions[rep - 1]()
                else:
                    if actions[rep - 1] is sys.exit:
                        sys.exit()
                    else:
                        actions[rep - 1](object_)
            except IndexError:
                pass
            except TypeError:
                # breadcrumbs.pop()
                break


def main_menu() -> None:
    """Définition du menu principal"""
    menu(PRINCIPAL, ACTION_PPAL)


def person_menu() -> None:
    """Définition du menu des joueurs potentiels"""
    menu(PERSON, ACTION_PERS_MENU)


def person_edit() -> None:
    """Fonction qui met à jour le classement d'une personne en fonction de son id"""
    person = library_controller.get(
        "Identifiant de la personne", person_controller.PersonController
    )
    if person:
        person_controller.edit(person)
    else:
        return


def tournament_menu() -> None:
    """Définition du menu des tournois"""
    menu(TOURNAMENT, ACTION_TOUR_MENU)


def tournament_submenu() -> None:
    """Définition du sous-menu tournoi pour un tournoi spécifique"""
    tournament = library_controller.get(
        "Identifiant du tournoi", tournament_controller.TournamentController
    )
    if tournament:
        menu(TOURNAMENT_MENU, ACTION_TOUR_SUBMENU, tournament)
    else:
        return


if __name__ == "__main__":
    with session:
        ACTION_PPAL = (tournament_menu, person_menu, sys.exit)
        ACTION_PERS_MENU = (
            person_controller.getall_orderby_name,
            person_controller.getall_orderby_elo,
            person_controller.add,
            person_edit,
            "",
            sys.exit,
        )
        ACTION_TOUR_MENU = (
            tournament_controller.TournamentController.get_all,
            tournament_controller.add,
            tournament_submenu,
            "",
            sys.exit,
        )
        ACTION_TOUR_SUBMENU = (
            tournament_controller.TournamentController.play,
            tournament_controller.addplayers,
            tournament_controller.TournamentController.removeplayer,
            tournament_controller.getall_players_orderby_name,
            tournament_controller.getall_players_orderby_elo,
            tournament_controller.TournamentController.getall_rounds,
            tournament_controller.getall_matchs,
            "",
            sys.exit,
        )

        main_menu()
